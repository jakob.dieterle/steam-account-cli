import requests
import time
from xml.etree import ElementTree
import pyfiglet
from termcolor import colored, cprint
import os
import json
import argparse
import configparser


def display(inv_value, api_key, steam_user_id, verbose=False, single=False):

    status_dict = {
            0 : colored("offline", "red"),
            1 : colored("online", "green"),
            2 : colored("busy", "yellow"),
            3 : colored("away", "yellow"),
            4 : colored("snooze", "grey"),
            5 : colored("looking to trade", "grey"),
            6 : colored("looking to play", "grey")
        }
    
    os.system('cls' if os.name == 'nt' else 'clear')
    
    # update inventory value only at the beginning
    inv_value = getInvValue(steam_user_id, inv_value, verbose)

    # update game stats only at the beginning
    if verbose:
        print("retrieving user game stats ...", end=" ")
    csgo = requests.get("http://api.steampowered.com/ISteamUserStats/"+
            "GetUserStatsForGame/v0002/?appid=730&key="
            +api_key+"&steamid="+steam_user_id)
    if verbose:
        print(csgo.status_code)

    if (csgo.status_code >= 300):
        print(colored("Error: retrieving game stats failed. Check internet"+
            " connection and keys in the config file!", "red"))
        return

    csgo_data = csgo.json()["playerstats"]["stats"]
    kd = csgo_data[0]["value"] / csgo_data[1]["value"]
    kd = "{:.2f}".format(kd)
    hs = csgo_data[25]["value"] / csgo_data[0]["value"]
    hs = "{:.2f}".format(hs)
    acc = csgo_data[41]["value"] / csgo_data[42]["value"]
    acc = "{:.2f}".format(acc)
    wins = csgo_data[109]["value"]
    rounds = csgo_data[80]["value"]
    k = csgo_data[82]["value"]
    d = csgo_data[83]["value"]
    mvp = csgo_data[84]["value"]
    sc = csgo_data[115]["value"]

    while True:

        # update user status each time
        if verbose:
            print("retrieving user status ...", end=" ")
        r = requests.get("http://api.steampowered.com/ISteamUser/"+
                "GetPlayerSummaries/v0002/?key="+
                api_key+"&steamids="+steam_user_id)
        

        if (r.status_code >= 200 and r.status_code < 300):
            if verbose:
                print(r.status_code)
            data = r.json()
            profile = data["response"]["players"][0]
            name = profile["personaname"]
            if verbose:
                print("successfully loaded player profile of: " + name)
            name_art = pyfiglet.figlet_format(name)
            clan_id = profile["primaryclanid"]
            clan_name = ""

            if (clan_id):
                if verbose:
                    print("retrieving clan name ...", end=" ")
                r_clan = requests.get("http://steamcommunity.com/gid/" + 
                        clan_id + "/memberslistxml/?xml=1")
                response_xml_as_string = r_clan.content
                root = ElementTree.fromstring(response_xml_as_string)
                clan_name = root[1][0].text
                if verbose:
                    print(r_clan.status_code)

            print("\n")
            if (int(profile["communityvisibilitystate"]) == 3):

                print(name_art)

                if (clan_name):
                    print("---<["+clan_name+"]>---")
                    
                status = status_dict[profile["personastate"]]
                print("player status: " + status + "\n")
                ### csgo game data
                print("[csgo stats]")
                print("last match:", end=" ")
                if (rounds == 16):
                    print(colored("win", "green"), end=" ")
                else:
                    print(colored("loss", "red"), end=" ")
                print("("+str(wins)+" wins)", end="\t\t")
                print("k/d: " + kd + ", hs%: " + hs + ", acc: " + acc)
                
                print("k: " + str(k) + " d: " + str(d) + " mvp: " +
                        str(mvp) + " score: " + str(sc), end="\t\t")

                if (float(inv_value) > -1):
                    print("csgo inventory value: " + str(inv_value) + " USD")
                    print("")
                else:
                    print("currently no inventory data")

            else:
                print("no acces to profile " + profile["personaname"])
        else:
            print(colored("Error: retrieving user status failed. Check internet"+
                " connection and keys in the config file!", "red"))
            return
        if single:
            break
        time.sleep(10)
        os.system('cls' if os.name == 'nt' else 'clear')


def getInvValue(steamid, inv_value, verbose=False):
    if (inv_value == -1):
        if verbose:
            print("retrieving inventory value ...", end=" ")
        data = requests.get("http://csgobackpack.net/api/"+
            "GetInventoryValue/?id={}".format(steamid))
        if verbose:
            print(data.status_code)
        if (data.status_code >= 200 or data.status_code < 300):
            data_json = data.json()
            if ('value' in data_json.keys()):
                inv_value = data_json["value"]
                if verbose:
                    print("successfully loaded inventory value.")
            else:
                print(colored("inventory value api rejected: exceeded maximum number of requests", "red"))
        
        if (data.status_code >= 300):
            print(colored("Error: retrieving inventory value failed. Check "+
                "internet connection and keys in the config file!", "red"))
            return

    else:
        if verbose:
            print("inventory value already loaded.")
    return inv_value


def main():

    config = configparser.ConfigParser()
    config.read('config.ini')
    api_key = str(config['DEFAULT']['api_key'])
    steam_user_id = str(config['DEFAULT']['steam_user_id'])

    parser = argparse.ArgumentParser(
      prog='steam_cli',
      formatter_class=argparse.RawDescriptionHelpFormatter,
      epilog='''
configuration:
  You need to add your api-key and steam-user-id
  to the config.ini file. Otherwise the script wont work.
         ''')
    parser.add_argument("-v", "--verbose", help="verbose mode",
            action="store_true")
    parser.add_argument("-s", "--single", help="only print once, no updating",
            action="store_true")
    args = vars(parser.parse_args())
    verbose = args["verbose"]
    single = args["single"]
    print(single)

    inv_value = -1
    display(inv_value, api_key, steam_user_id, verbose, single)

if __name__ == "__main__":
    main()


